= Setting up a local Fedora authoring environment
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v0.0.1, 2022-09-26

[abstract]
____
This guide describes step by step the setup of a local Fedora authoring environment. Workstations with Linux and macOS are directly supported. A graphical user interface is helpful, but not an requirement. 
____

== How it works

The Fedora publishing system uses plain text files with AsciiDoc formatting for all content. Therefore, you need a text editor to work on it.

Fedora uses git repositories for file storage and version control. Therefore, git must be installed on the workstation. Editing is then done in the git working directory. 

In the course of work, one would like to check the formatting and appearance. This requires a minimal display system.  Every Fedora docs repository provides this, but it requires a Docker or Podman container.  

== Prerequisites 

=== Plain text editor

Basically, you can use any plain text editor you are used to, among others [application]`vim`, [application]`emacs`, [application]`Atom`, [application]`VSCode`, etc.

Very helpful is AsciiDoc syntax highlighting and also already a rough preview of the formatted text. Here the [application]`Atom` program editor and the special AsciiDoc editor [application]`AsciiDocFX` stand out. Both provide an optional split screen containing a rough preview. [application]`AsciiDocFX` provides additionally a editing menue as it is known by text processing software.

Both are FOSS projects and make the software freely available. The https://atom.io/[Atom project] provides the editor for Linux, macOS and Windows. The https://asciidocfx.com/[AsciiDocFX project] provides a Java binary as Linux, macOS or Windows installation file that furthermore works on any operating system with a Jave runtime environment. 

=== Git

The git version control system is available for virtually all operating systems. On a Fedora workstation it is installed via dnf. We use the command line only tool here. 

[source,]
----
[…]$ sudo dnf install git
----

For other operating systems, use the appropriate installation files.

=== Container

The preview system requires a container. The container is set up automatically, but a container runtime environment must be available and active. 

On a Fedora system, simply install Podman from the Fedora repository.
[source,]
----
[…]$ sudo dnf install podman
----

On macOS or Windows, install Docker CE of the https://www.docker.com/[Docker project]. 

== Setting up a documentation project

Coming soon

