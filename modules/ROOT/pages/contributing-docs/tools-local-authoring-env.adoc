= How to Create and Use a Local Fedora Documentation Workflow
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v1.0, 2022-12-16

[abstract]
____
In a local environment, you can create or edit your documents offline. You use a local git repository that contains a complete set of documents and tools to edit. After editing is complete, preview them in a local build of Docs pages. It is by far the most flexible way of working with Docs repositories, enabled by the extensive adaptation to individual work routines and work equipment.
____

A local writing environment allows full access to all the resources you routinely use on your workstation and is therefore perfectly adaptable to your style of working. It is particularly suitable for the creation of a completely new set of documentation on a topic or for revision of an existing set of documentation.
	
== Prerequisites 

1. You need to fulfill the basic requirements as specified in xref:contributing-docs/index.adoc#prerequisites[Write contributions to Docs].

2. The workflow uses command line tools in most steps of the process. Having some experience working in the Linux Terminal is therefore necessary or must be learned in parallel.

== Setting up an authoring environment

=== Preparations

1. Use SSH keys to authenticate to the GitLab remote server.

2. A GPG (GNU Privacy Guard) key is to sign the commits verified.

On GitLab, go to Preferences - User Settings - Add an SSH Key / a GPG Key

Git and Podman come with Fedora workstation as default. The Podman container is set up automatically.

Create a local subdirectory where the clone of Docs repo is stored

Create the main directory with mkdir DirectoryName

----
$ mkdir ~/FedoraDocs
----

=== Set up the repository  

*Fork a project*

In GitLab, go to the upstream repository and select the *Fork* button at the top. Select a namespace (your GitLab ID) and a visibility level for the forked project, which creates a fork to your GitLab account. 

If you don't have access to create a project, open an issue ticket in GitLab to create one for you.

*Clone the repository*

In your forked repo, select the blue *Clone* button drop-down, and then select *Clone with SSH*. Copy that link to your clipboard.

Go to your main docs directory, and clone the repo.

----
$ cd FedoraDocs
----

When cloning a repository, you can specify the new directory name as an additional argument.
This creates a ContributorsGuide subdirectory and clones the repo into that directory.

----
$ git clone <GIT URL> ContributorsGuide
----

----
$ cd ContributorsGuide
----

Check the current branch, which is main or master.
----
$ git status
----

Show all branches on the remote.
----
$ git branch -v -a
----

To switch branches,
----
$ git checkout branch_name
----

== Working on content

Open the file(s) with your choice of text editor, edit, and save.

=== Sync your fork with the upstream repository

----
$ git remote -v
----

This will show the fork in your account as the origin.

Go to the upstream repo, and select the Clone button drop-down, and copy the Clone with SSH info.

----
$ git remote add upstream <SSH URL>
----

Run the command below to show both origin and upstream.

----
$ git remote -v
----

To keep your fork in sync with upstream,

----
$ git checkout main
$ git fetch upstream
----

----
$ git pull upstream main
----
This command gets most recent commits from upstream to your local branch.

----
$ git push origin main
----

This command pushes to your fork in GitLab, and refresh in GitLab to see the latest changes from upstream in your fork.

=== Run the scripts to preview locally

Go to the the directory where cloned repo is, and run the build scripts in terminal.

----
$ ./build.sh && ./preview.sh
----

Preview in your browser using the address localhost:8080

If you see the docsbuilder.sh script on the upstream repo, use it.

=== Save your work

To see what has been going on with git add and git commit commands.

----
$ git status
----

To specify which files will be committed,

----
$ git add <file> 
----

Alternatively, to add all the files in the current working directory,

----
$ git add .
----

When you commit, always include a clear commit message.

----
$ git commit -m "commit message"
----

If you committed with wrong messages,

----
$ git commit --amend -m "new commit message"
----

The git log command displays the project history and specific changes.

----
$ git log
----

Push to your fork in GitLab, refresh in GitLab to see the latest changes from upstream in your fork.

----
$ git push origin
----

=== Create merge requests

Go to your fork of the repository in GitLab. 

On the left menu, click Merge requests, and select the blue *New merge request* in the middle.

When you work in a fork, select your fork of the repository as the source branch.

In the Target branch dropdown list, select the branch from the upstream repository as the target branch.

Click Compare branches and continue.

Select Create merge request.
